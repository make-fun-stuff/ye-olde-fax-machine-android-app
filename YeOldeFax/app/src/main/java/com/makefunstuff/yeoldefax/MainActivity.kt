package com.makefunstuff.yeoldefax

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.makefunstuff.yeoldefax.databinding.ActivityMainBinding
import android.widget.Toast
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Spinner
import android.widget.Button
import androidx.core.view.isVisible
import org.eclipse.paho.client.mqttv3.*
import java.util.*
import java.util.Arrays.asList

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_settings, R.id.navigation_message, R.id.navigation_draw))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

    }

    fun getUrl(getDefault: Boolean = false): String {
        val defaultValue = getString(R.string.default_mqtt_url)
        if (getDefault) {
            return defaultValue
        }
        val preferences: SharedPreferences = this@MainActivity.getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE)
        return preferences.getString(getString(R.string.url_key), defaultValue) ?: return defaultValue
    }

    fun getPort(getDefault: Boolean = false): String {
        val defaultValue = getString(R.string.default_mqtt_port)
        if (getDefault) {
            return defaultValue
        }
        val preferences: SharedPreferences = this@MainActivity.getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE)
        return preferences.getString(getString(R.string.port_key), defaultValue) ?: return defaultValue
    }

    fun getProtocol(getDefault: Boolean = false): String {
        val defaultValue = getString(R.string.default_mqtt_protocol)
        if (getDefault) {
            return defaultValue
        }
        val preferences: SharedPreferences = this@MainActivity.getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE)
        return preferences.getString(getString(R.string.protocol_key), defaultValue) ?: return defaultValue
    }

    fun getProtocolSelection(getDefault: Boolean = false): Int {
        if (getDefault) {
            return 0
        }
        val preferences: SharedPreferences = this@MainActivity.getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE)
        val value = preferences.getString(getString(R.string.protocol_key), getString(R.string.default_mqtt_protocol))
        return asList(*resources.getStringArray(R.array.mqtt_protocol_options)).indexOf(value)
    }

    fun getTopic(getDefault: Boolean = false): String {
        val defaultValue = getString(R.string.default_mqtt_topic)
        if (getDefault) {
            return defaultValue
        }
        val preferences: SharedPreferences = this@MainActivity.getSharedPreferences(
            getString(R.string.preference_file),
            Context.MODE_PRIVATE
        )
        return preferences.getString(getString(R.string.topic_key), defaultValue)
            ?: return defaultValue
    }

    fun saveSettings(v: View) {
        val view = v.rootView
        Log.d("Settings", "Reading values from input fields")
        var url = (view.findViewById<View>(R.id.mqtt_url) as EditText).text.toString()
        Log.d("Settings", "Url: $url")
        var port = (view.findViewById<View>(R.id.mqtt_port) as EditText).text.toString()
        Log.d("Settings", "Port: $port")
        var protocol = (view.findViewById<View>(R.id.mqtt_protocol) as Spinner).selectedItem.toString()
        Log.d("Settings", "Protocol: $protocol")
        val topic = (view.findViewById<View>(R.id.mqtt_topic) as EditText).text.toString()
        Log.d("Settings", "Topic: $topic")
        if (url.isEmpty() || port.isEmpty() || protocol.isEmpty() || topic.isEmpty()) {
            Toast.makeText(this@MainActivity, "Missing required fields!", Toast.LENGTH_SHORT).show()
            return
        }
        Log.d("Settings", "Url: $url, Port: $port, Protocol: $protocol, Topic: $topic")
        val sharedPref = this@MainActivity.getSharedPreferences(getString(R.string.preference_file), Context.MODE_PRIVATE)
            ?: return
        with (sharedPref.edit()) {
            putString(getString(com.makefunstuff.yeoldefax.R.string.url_key), url)
            putString(getString(com.makefunstuff.yeoldefax.R.string.port_key), port)
            putString(getString(com.makefunstuff.yeoldefax.R.string.protocol_key), protocol)
            putString(getString(com.makefunstuff.yeoldefax.R.string.topic_key), topic)
            apply()
        }
        Toast.makeText(this@MainActivity, "Saved", Toast.LENGTH_SHORT).show()
    }

    private fun setLoadingMode(loading: Boolean, editText: EditText, button: Button, loader: ProgressBar) {
        editText.isEnabled = !loading
        button.isEnabled = !loading
        loader.isVisible = loading
    }

    fun cleanMessage(message: String): String {
        return message;
    }

    fun sendMessage(v: View) {
        Log.d("Publish","Clicked 'Send'")

        val editText = findViewById<View>(R.id.message_input) as EditText
        val button = findViewById<View>(R.id.send_message_button) as Button
        val loader = findViewById<View>(R.id.send_message_loader) as ProgressBar

        val message = cleanMessage(editText.text.toString())
        if (message.isEmpty()) {
            Toast.makeText(this@MainActivity, "Cannot send an empty message!", Toast.LENGTH_SHORT).show()
            return
        }

        var uri = getProtocol() + "://" + getUrl() + ":" + getPort()
        var client = MQTTClient(this@MainActivity, uri, "YeOldeFax")

        setLoadingMode(true, editText, button, loader)
        client.connect(object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("Publish", "Connection success")
                    client.publish(getTopic(),
                        message,
                        1,
                        false,
                        object : IMqttActionListener {
                            override fun onSuccess(asyncActionToken: IMqttToken?) {
                                Log.d("Publish", "Published message to topic")
                                setLoadingMode(false, editText, button, loader)
                                editText.text.clear()
                                Toast.makeText(this@MainActivity, "Published message!", Toast.LENGTH_SHORT).show()
                                client.disconnect()
                            }

                            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                                Log.d("Publish", "Failed to publish message to topic")
                                setLoadingMode(false, editText, button, loader)
                                Toast.makeText(this@MainActivity, "Failed to publish message", Toast.LENGTH_LONG).show()
                            }
                        })
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("Publish", "Connection failure: ${exception.toString()}")
                    Toast.makeText(this@MainActivity, "Failed to connect: ${exception.toString()}", Toast.LENGTH_LONG).show()
                }
            },
            object : MqttCallback {
                override fun messageArrived(topic: String?, message: MqttMessage?) {}

                override fun connectionLost(cause: Throwable?) {
                    Log.d("Publish", "Connection lost ${cause.toString()}")
                }

                override fun deliveryComplete(token: IMqttDeliveryToken?) {
                    Log.d("Publish", "Delivery complete")
                }
            })

    }
}
