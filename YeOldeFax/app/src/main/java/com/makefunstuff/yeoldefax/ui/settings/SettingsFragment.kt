package com.makefunstuff.yeoldefax.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.makefunstuff.yeoldefax.MainActivity
import com.makefunstuff.yeoldefax.R
import com.makefunstuff.yeoldefax.databinding.FragmentSettingsBinding
import java.util.*

class SettingsFragment : Fragment() {

    private lateinit var settingsViewModel: SettingsViewModel
    private var _binding: FragmentSettingsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        settingsViewModel =
            ViewModelProvider(this).get(SettingsViewModel::class.java)

        _binding = FragmentSettingsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        populateSettingsFields()
    }

    private fun populateSettingsFields(getDefaults: Boolean = false) {
        val act = activity as MainActivity
        (view?.findViewById<View>(R.id.mqtt_url) as EditText).setText(act.getUrl(getDefaults))
        (view?.findViewById<View>(R.id.mqtt_port) as EditText).setText(act.getPort(getDefaults))
        (view?.findViewById<View>(R.id.mqtt_protocol) as Spinner).setSelection(act.getProtocolSelection(getDefaults))
        (view?.findViewById<View>(R.id.mqtt_topic) as EditText).setText(act.getTopic(getDefaults))
    }


}